/*
 * malloc, realloc, free を大量に呼ぶ時間を計測するプログラム。
 *
 * malloc に使用するライブラリを入れ替えることにより
 * 処理時間に変化が現れることを確認するために使用する。
 * 実際のアプリケーションでどのライブラリを使用するのが良いかはアプリケーションによる。
 */
#include <algorithm>
#include <cassert>
#include <chrono>
#include <concepts>
#include <cstddef>
#include <cstdlib>
#include <format>
#include <iostream>
#include <iterator>
#include <random>
#include <ratio>
#include <string_view>
#include <vector>

#ifndef NDEBUG
constexpr std::size_t num_repetitions = 10U;
#else
constexpr std::size_t num_repetitions = 100U;
#endif

constexpr std::size_t num_buffers = 1000U;
constexpr std::size_t max_size = 1024U * 1024U;
constexpr std::size_t min_size = 1U;

[[nodiscard]] static std::vector<std::size_t> create_size_list(
    std::mt19937& generator) {
    std::vector<std::size_t> size_list;
    size_list.reserve(num_buffers);
    std::uniform_int_distribution<std::size_t> distribution{min_size, max_size};
    std::generate_n(std::back_inserter(size_list), num_buffers,
        [&] { return distribution(generator); });
    return size_list;
}

[[nodiscard]] static double to_usec(
    std::chrono::steady_clock::duration duration) {
    return std::chrono::duration_cast<
        std::chrono::duration<double, std::micro>>(duration)
        .count();
}

class timer {
public:
    explicit timer(std::string_view name) : name_(name) {}

    template <std::invocable<> Function>
    void measure(Function&& function) {
        const auto start = std::chrono::steady_clock::now();
        function();
        const auto end = std::chrono::steady_clock::now();
        const auto time = end - start;
        if (time < min_time_) {
            min_time_ = time;
        }
        if (time > max_time_) {
            max_time_ = time;
        }
        total_time_ += time;
    }

    void show_results() {
        std::cout << std::format(R"({}:
    min: {: 13.3f}
    max: {: 13.3f}
    ave: {: 13.3f}
)",
            name_, to_usec(min_time_), to_usec(max_time_),
            to_usec(total_time_) / static_cast<double>(num_repetitions));
    }

private:
    std::string_view name_;
    std::chrono::steady_clock::duration min_time_{
        std::chrono::steady_clock::duration::max()};
    std::chrono::steady_clock::duration max_time_{
        std::chrono::steady_clock::duration::min()};
    std::chrono::steady_clock::duration total_time_{
        std::chrono::steady_clock::duration(0)};
};

int main() {
    const auto start = std::chrono::steady_clock::now();

    // 条件をそろえるためデフォルトのシード値に固定する。
    std::mt19937 generator{};
    const auto first_size_list = create_size_list(generator);
    const auto second_size_list = create_size_list(generator);

    std::vector<void*> pointer_list;
    pointer_list.resize(num_buffers);

    timer malloc_timer("malloc");
    timer realloc_timer("realloc");
    timer free_timer("free");
    for (std::size_t repetition = 0; repetition < num_repetitions;
         ++repetition) {
        malloc_timer.measure([&] {
            for (std::size_t i = 0; i < num_buffers; ++i) {
                pointer_list[i] = std::malloc(first_size_list[i]);
                assert(pointer_list[i] != nullptr);
            }
        });

        realloc_timer.measure([&] {
            for (std::size_t i = 0; i < num_buffers; ++i) {
                pointer_list[i] =
                    std::realloc(pointer_list[i], second_size_list[i]);
                assert(pointer_list[i] != nullptr);
            }
        });

        free_timer.measure([&] {
            for (std::size_t i = 0; i < num_buffers; ++i) {
                std::free(pointer_list[i]);
            }
        });
    }

    malloc_timer.show_results();
    realloc_timer.show_results();
    free_timer.show_results();

    const auto end = std::chrono::steady_clock::now();
    const auto total_duration = end - start;
    std::cout << std::format(
                     "Total execution time: {}", to_usec(total_duration))
              << std::endl;

    return 0;
}
