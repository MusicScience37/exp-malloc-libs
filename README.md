# malloc を置き換えるライブラリの動作確認

## 動作結果の例

### デフォルト（glibc）

```console
$ ./build/Release/bin/malloc_bench
malloc:
    min:      1773.622
    max:      3056.077
    ave:      1929.048
realloc:
    min:     10787.889
    max:    149162.097
    ave:     99800.347
free:
    min:      3613.301
    max:     10460.846
    ave:      7497.435
Total execution time: 10922801.64
```

- realloc はメモリ確保と既存のデータの移行までするから malloc より時間かかる。
  malloc してから memcpy するよりは速いはず。

### tcmalloc （Google 社製の malloc）

```console
$ LD_PRELOAD=/usr/local/lib/libtcmalloc.so ./build/Release/bin/malloc_bench
malloc:
    min:       404.925
    max:      3255.341
    ave:       521.533
realloc:
    min:     22600.912
    max:    148260.444
    ave:     26621.216
free:
    min:       417.890
    max:       873.245
    ave:       523.475
Total execution time: 2766743.522
```

### jemalloc （Meta 社製の malloc）

```console
$ LD_PRELOAD=/usr/local/lib/libjemalloc.so ./build/Release/bin/malloc_bench
malloc:
    min:       348.649
    max:       839.133
    ave:       412.349
realloc:
    min:     20964.154
    max:    103883.852
    ave:     39683.647
free:
    min:      1122.868
    max:      6519.933
    ave:      2705.908
Total execution time: 4280314.474
```

### mimalloc （Microsoft 社製の malloc）

```console
$ LD_PRELOAD=/usr/local/lib/libmimalloc.so ./build/Release/bin/malloc_bench
malloc:
    min:       183.386
    max:      2487.246
    ave:       278.234
realloc:
    min:     16773.422
    max:    129892.831
    ave:     22912.509
free:
    min:       212.965
    max:       957.360
    ave:       430.084
Total execution time: 2362222.962
```
